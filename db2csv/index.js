const express = require('express');
const app = express();
const oracledb = require('oracledb');
var bodyparser = require('body-parser');
const Json2csvParser = require("json2csv").Parser;
const fs = require('fs');

app.use(bodyparser.json());
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;

app.get('/selectall',async function(req, res){
    try{
        
        var connection = await oracledb.getConnection({
            user : 'kv11',
            password : 'kv_credence',
            connectString : '192.168.1.94/srorcl'
        });
        var result = await connection.execute(`select * from emp`);

        const jsonData = JSON.parse(JSON.stringify(result.rows))
        console.log("jsonData", jsonData);

         const json2csvParser = new Json2csvParser({ header: true});
         const csv = json2csvParser.parse(jsonData);
         console.log(csv)
         res.send("Write to data.csv successfully");
         fs.writeFile("data.csv", csv, function(error) {
            if (error) throw error;
            console.log("Write to Data.csv successfully!");
        });
    }
    catch(err){
        console.log("ERROR: ", err);
    }
    finally{
        connection.close();
    }
});

app.listen(3000, () =>{
    console.log("Listening to port 3000...");
})