const supertest =  require('supertest');
describe('POST /uploadData', function(){
    it('Name validation',function(done){
        supertest('http://localhost:3001')
        .post('/uploadData')
        .send(
            [{
                "name":"sushma90",
                "age":26,
                "gender":"F"
            }]
        )
        .expect(200,done);
    });
    it('Age validation',function(done){
        supertest('http://localhost:3001')
        .post('/uploadData')
        .send(
            [{
                "name":"sushma",
                "age":"d26",
                "gender":"F"
            }]
        )
        .expect(200,done);
    });

    it('Gender validation',function(done){
        supertest('http://localhost:3001')
        .post('/uploadData')
        .send(
            [{
                "name":"sushma",
                "age":26,
                "gender":"M78"
            }]
        )
        .expect(200,done);
    });

    it('Data validation',function(done){
        supertest('http://localhost:3001')
        .post('/uploadData')
        .send(
            [{
                "name":"sushma",
                "age":26,
                "gender":"F"
            }]
        )
        .expect(200,done);
    });
});

describe('GET /sort', function(){
    it('Fetch all data',function(done){
        supertest('http://localhost:3001')
        .get('/sort')
        .expect(200,done);
    });
});
