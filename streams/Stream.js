const Stream = require('stream')

const readableStream = new Stream.Readable('data.txt')
const writableStream = new Stream.Writable('vsvd.txt')

writableStream._write = (chunk, encoding, next) => {
    console.log(chunk.toString())
    next()
}

readableStream.pipe(writableStream)

readableStream.push('ping!')
readableStream.push('pong!')

writableStream.end()